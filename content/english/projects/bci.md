---
title: "BCI Project"
date: 2020-06-25T18:24:14+01:00
author: Kateland Bobyn
image: images/blog/bci_banner.jpg
description: "Communicating with a robot via EEG headset signals"
---

The BCI project was launched just last year and is still getting underway. We are currently exploring how we can use machine learning to classify signals from EEG headsets.

## The Plan

A meeting will be held at the beginning of the academic year to discuss project ideas. Participants are encouraged to bring their own ideas for discussion, but if nobody has anything in mind, the team has a few different project ideas to choose from.

Weekly meetings will be held for each of the teams to discuss progress. The team leaders will also be encouraged to attend general meetings so we can ensure a smooth integration process.

## Who We're Looking For

The nature of this project means that we are looking for participants with a wide range of skills. All our teams are open to anyone, but as an indication:

- Our **Data Collection** team focuses on recording EEG signals in the right experimental settings. Some background in psychology or neuroscience would be helpful, but training will be provided on how to use the headset and ensure optimal accuracy.
- The data then gets passed onto our **Signal Processing** team, who could benefit from Electronics Engineering or ACSE students.
- The **Machine Learning** team uses AI techniques on the processed data to classify the signals into outputs that are useful for the application. Computer Science students or anyone with programming skills could help out here. We will be working closely with the Sheffield AI Society.
- We will be forming a **Hardware** team to build the “prosthetic” component of our project. Past initiatives have been inspired by NeuroTechX projects at other universities, e.g., a wheelchair controlled by the brain. Anyone with access to the iForge, CAD skills, and electronics skills would be helpful here!

If you are interested in leading any of these teams, please reach out to Kate.

## Sponsorship

As a fledgling project, we are looking for all the support we can get. We are currently using an Emotiv EPOC+ headset on an experimental basis, but are looking to purchase new equipment and licenses. If you would like to cooperate with us, please contact Kate!
