---
title: "FAQ"
date: 2020-10-22T17:51:44+01:00
author: Bhoomika Gandhi
image: images/blog/faq.jpg
description: "Frequently Asked Questions"
---

How do I join the society? 
: You may join our society via the SU website. We have a £3 membership fee for the year. 

Do I need any experience for the projects?
: No prior experience is required, however, if during a project you feel the need to learn some new skills that would be useful, we’d encourage this. You can also attend Bootcamp sessions for this.

Where do I find information about the sessions?
: We send weekly emails and post updates on facebook with information about the sessions. If you aren’t on facebook and/or aren’t receiving emails, please connect us and let us know. 

What would my key responsibilities be in either of the 4 projects?
: All the projects are at different stages at the moment so they can vary quite significantly. The best way to find out more about this would be by contacting the project leaders directly. 

Do I need to be present for every project session? 
: While it is not absolutely compulsory that you attend every session, it is encouraged. We’re also in the middle of providing HEAR recognition and in order to get this, you need to attend a certain number of sessions in the year.

I haven’t been receiving any email updates, what do I do?
: We’re having some issues with the SU memberships at the moment but we’re trying to sort this soon as possible. We also post our updates on facebook. However, if you’re still not getting emails and/or would prefer them, don’t hesitate to email the society about it. We can manually add you to a mailing list.

Are all the sessions this academic year (2020-2021)  going to be virtual? 
: According to the government and SU guidelines, we’ve decided to keep all the sessions virtual in the Autumn semester. We are planning on having face to face activities in the Spring semester, however, this is subject to change depending on the situation. 