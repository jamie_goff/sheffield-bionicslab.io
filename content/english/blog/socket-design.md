---
title: "Arm: Socket Design"
date: 2020-08-25T19:44:00+01:00
author: Alex Pop & Grace Faulkner
image: images/blog/socket-front.png
description: "Socket design for the arm completed recently by Alex Pop"
---

The socket is an essential part of the design as it is where the residual limb and the bionic arm interact. As such, the socket must be secure to allow the arm to be used while also being comfortable, otherwise it can be incredibly painful for the pilot to use.

Personalized sockets are essential to solving these issues, something we are able to provide at a much more affordable price when compared to existing companies. The original plan was to use Mark's CT scans, but when the team couldn't get access, we had to think creatively.

In order to make a socket specific to Mark, Arm Team Member and Society President, Alex Pop, has taken over 800 pictures of Mark's residual limb from a variety of angles. These were then analysed in a software called 3D Zephyr and after over 24 hours of rendering time a mesh was created. This was cleaned up using Blender thus creating a 3D model of the limb; which in turn was used to create a personalized socket. This allowed us to utilise a number of new techniques and software packages thus broadening our knowledge and skill set.

Additionally, Alex also had to factor in other problems. The arm will be controlled by a Myoband, essentially a ring of sensors which detect muscle activity. These sensors must make direct contact with the skin to be reliable and so the design for the socket must account for this thus limiting available surface to grip onto.

{{<figure src="/images/blog/socket-side.png" caption="Socket Design by Alex Pop" width="50%">}}
