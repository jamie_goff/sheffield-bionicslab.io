---
title: "Interview With Mark Whitehead"
date: 2020-07-20T13:01:35+01:00
author: Thomas Binu Thomas
image: images/blog/interview.jpg
description: "An interview with Mark Whitehead"
---

# Interview with Mark Whitehead

Our interactions with Mark Whitehead has been very eyeopening and motivational. From these, we learned about how we take simple things like tying shoelaces or spreading butter on toast for granted. We also got reminded why we do what we do. On the 13th of July 2020 we had an interview between Mark and Thomas, the arm project leader.

{{< youtube scmkXJ5-vBU>}}

_Thank you everyone who has donated and will donate!_

GoFundMe page link: <https://gf.me/u/ydvr48>
